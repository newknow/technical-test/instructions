# Instructions

Back-end: https://gitlab.com/newknow/technical-test/users-service

Front-end: https://gitlab.com/newknow/technical-test/front-end

- Fork the repository and work on that fork. You can do it on Github if you don't have a Gitlab account and don't want to create one.
- Tasks are listed on the README file of each project. This is just some suggestions, feel free to do something outside of the suggestions if you feel like it.
- **DO NOT complete** all the tasks. 
- Spend **2 to 3 hours max**
- Privilege **quality over quantity**
- No need to "time" your test, feel free to start, take pauses, and restart later
- Thus, DO NOT HESITATE to ask us any question. We're expecting our teammates to be proactive and ask question so, feel free to do so
- There is NO TRICK in the exercice, if something seems to be odd, do not hesitate to ask questions


# Send us back
- The link to the forked repo
- A small explaination of what has been done (few bullets points)
- Few lines to justify some decisions that you had to make, in an additional .md file. 

# Notes
- **Enjoy!**